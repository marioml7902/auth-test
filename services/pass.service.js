'use strict'

const bcrypt = require('bcrypt');

//encriptapassword
//
//devuelve un hash con un salt incluido en el formato




function encriptapassword( password ){
    return bcrypt.hash(password, 10);
}

//comparaPassword
//
//Devolver verdadero o falso si coinciden o no el pass y el hash

function comparaPassword( password, hash){
    return bcrypt.compare(password, hash);
}

module.exports = {
    encriptapassword,
    comparaPassword
};
