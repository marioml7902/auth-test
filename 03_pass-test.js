'use strict'

const PassService = require('./services/pass.service');
const moment = require('moment');

//Datos para simulacion...
const miPass = "miContraseña";
const badPass = "miOtraContraseña";
const usuario = {
    _id: '123456789',
    email: 'mml148@gclous.ua.es',
    displayName: 'Mario Martinez',
    password: miPass,
    singUpDate : moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Encriptamos el password
PassService.encriptapassword( usuario.password)
    .then( hash => {
        usuario.password = hash;
        console.log(usuario);

        //Verificamos el pass
        PassService.comparaPassword( miPass, usuario.password)
        .then( isOk => {
            if(isOk){
                console.log('p1: El password es correctp');
            }else{
                console.log('p1: El pass no es correcto');
            }
        })
        .catch(err => console.log(err));

        //Verificamos el pass contra un pass falso
        PassService.comparaPassword( badPass, usuario.password)
        .then( isOk => {
            if(isOk){
                console.log('p2: El password es correctp');
            }else{
                console.log('p2: El pass no es correcto');
            }
        })
        .catch(err => console.log(err));
    });
